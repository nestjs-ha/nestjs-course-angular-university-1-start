import { PipeTransform, ArgumentMetadata, BadRequestException } from '@nestjs/common';

export class ToIntegerPipe implements PipeTransform {

    transform(value: string, metadata: ArgumentMetadata): number {

        const val = parseInt( value, 10 );

        if ( isNaN(val) ) {
            throw new BadRequestException(`converstion to number failed ${value}`);
        }

        return val;
    }
}
