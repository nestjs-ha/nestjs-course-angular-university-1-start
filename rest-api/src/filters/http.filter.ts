import {ExceptionFilter, Catch, HttpException, ArgumentsHost } from '@nestjs/common';
import { Request, Response } from 'express';
import { url } from 'inspector';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {

    catch(exception: HttpException, host: ArgumentsHost): any {
        console.log('HTTP exception handler triggered', JSON.stringify(exception));

        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const statusCode = exception.getStatus();

        // send back and error payload
        return response.status(statusCode).json({
            status: statusCode,
            createdBy: 'HttpExceptionFilter',
            // message: exception.message.message,
            path: request.url,
        });
    }
}
