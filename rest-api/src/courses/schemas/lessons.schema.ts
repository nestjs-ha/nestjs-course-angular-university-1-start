import * as mongoose from 'mongoose';
import { ObjectId } from 'bson';

export const LessonSchema = new mongoose.Schema({
//   _id: ObjectId,
  description: String,
  duration: String,
  seqNo: Number,
  course: ObjectId
});