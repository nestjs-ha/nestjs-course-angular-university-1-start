import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Course } from '../../../shared/course';

@Injectable()
export class CoursesRepository {

    constructor(@Inject('COURSE_MODEL')
                private courseModel: Model<Course> ) {
        // constructor(@InjectModel('Course') private courseModel: Model<Course> ) {
    }

    async findAll(): Promise<Course[]> {
       return await this.courseModel.find();
    }

    // syntax: filter, update, options
    async updateCourse( courseId: string, changes: Partial<Course> ): Promise<Course> {
        return await this.courseModel.findOneAndUpdate(
            { _id: courseId },
            changes,
            { new: true },
        );
    }

    async deleteCourse( courseId: string ) {
       return  this.courseModel.deleteOne({_id: courseId});
    }

    async addCourse( course: Partial<Course>): Promise<Course> {

        // create an in memory object with generated object id
        const newCourse = new this.courseModel( course );

        await newCourse.save();

        return newCourse.toObject({versionKey: false});
    }
}
