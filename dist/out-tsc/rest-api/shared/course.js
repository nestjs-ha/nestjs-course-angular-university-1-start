export function compareCourses(c1, c2) {
    const compare = c1.seqNo - c2.seqNo;
    if (compare > 0) {
        return 1;
    }
    else if (compare < 0) {
        return -1;
    }
    else {
        return 0;
    }
}
//# sourceMappingURL=course.js.map