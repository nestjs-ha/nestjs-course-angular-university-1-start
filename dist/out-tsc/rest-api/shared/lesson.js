export function compareLessons(l1, l2) {
    const compareCourses = l1.courseId - l2.courseId;
    if (compareCourses > 0) {
        return 1;
    }
    else if (compareCourses < 0) {
        return -1;
    }
    else {
        return l1.seqNo - l2.seqNo;
    }
}
//# sourceMappingURL=lesson.js.map