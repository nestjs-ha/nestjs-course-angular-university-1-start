var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { findAllCourses, findLessonsForCourse } from './db-data';
// import * as mongoose from 'mongoose';
import * as mongodb from 'mongodb';
import config from './src/config/keys';
console.log("Populating the MongoDB database with some sample data ...");
// const MongoClient = require('mongodb').MongoClient;
// const ObjectId = require('mongodb').ObjectID;
const MongoClient = mongodb.MongoClient;
const ObjectId = mongodb.ObjectID;
/*****************************************************************************************************
*
*
* IMPORTANT!!!
*
* MongoDB Connection URL - create your own url with the right cluster name, username, password and database name
*
* Format: mongodb+srv://username:password@clustername
*
* Example (don't use this as you don't have write access):
*
* mongodb+srv://nestjs:ZeEjdswOWHwoenQO@cluster0-dbucq.gcp.mongodb.net
*
*****************************************************************************************************/
// const MONGODB_CONNECTION_URL = 'mongodb+srv://nestjs:ZeEjdswOWHwoenQO@cluster0-dbucq.gcp.mongodb.net';
const MONGODB_CONNECTION_URL = config.mongoURI;
// Database Name
const dbName = 'nestjs-course';
// Create a new MongoClient
const client = new MongoClient(MONGODB_CONNECTION_URL);
// Use connect method to connect to the Server
client.connect((err, client) => __awaiter(this, void 0, void 0, function* () {
    try {
        if (err) {
            console.log('Error connecting to database, please check the username and password, exiting.');
            process.exit();
        }
        console.log('Connected correctly to server');
        const db = client.db(dbName);
        const courses = findAllCourses();
        // for (let i = 0; i < courses.length; i++) {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < courses.length; i++) {
            const course = courses[i];
            const newCourse = Object.assign({}, course);
            delete newCourse.id;
            console.log('Inserting course ', newCourse);
            const result = yield db.collection('courses').insertOne(newCourse);
            const courseId = result.insertedId;
            console.log('new course id', courseId);
            const lessons = findLessonsForCourse(course.id);
            // tslint:disable-next-line: prefer-for-of
            for (let j = 0; j < lessons.length; j++) {
                const lesson = lessons[j];
                const newLesson = Object.assign({}, lesson);
                delete newLesson.id;
                delete newLesson.courseId;
                newLesson.course = new ObjectId(courseId);
                console.log('Inserting lesson', newLesson);
                yield db.collection('lessons').insertOne(newLesson);
            }
        }
        console.log('Finished uploading data, creating indexes.');
        yield db.collection('courses').createIndex({ 'url': 1 }, { unique: true });
        console.log("Finished creating indexes, exiting.");
        client.close();
        process.exit();
    }
    catch (error) {
        console.log('Error caught, exiting: ', error);
        client.close();
        process.exit();
    }
}));
console.log('updloading data to MongoDB...');
process.stdin.resume();
//# sourceMappingURL=populate-db.js.map