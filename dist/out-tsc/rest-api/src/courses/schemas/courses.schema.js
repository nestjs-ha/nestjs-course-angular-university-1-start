import * as mongoose from 'mongoose';
// Note: mongoose property types using types of JavaScript runtime
export const CourseSchema = new mongoose.Schema({
    //   _id: ObjectId,
    seqNo: Number,
    description: String,
    longDescription: String,
    iconUrl: String,
    category: String,
    lessonCount: Number,
    url: String,
    promo: String,
});
//# sourceMappingURL=courses.schema.js.map