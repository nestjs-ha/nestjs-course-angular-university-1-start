var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { Controller, Get, Put, Param, Body, Delete, Post, HttpException } from '@nestjs/common';
import { CoursesRepository } from './repositories/courses.repository';
let CoursesController = class CoursesController {
    constructor(coursesDB) {
        this.coursesDB = coursesDB;
    }
    findAllCourses() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.coursesDB.findAll();
        });
    }
    updateCourse(courseId, changes) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('updating course');
            // _id field not allowed on Partial<Course>
            // @ts-ignore
            if (changes._id) {
                console.log('execption occurred!!!');
                throw new HttpException(`Can\'t update course id`, 400);
            }
            return this.coursesDB.updateCourse(courseId, changes);
        });
    }
    deleteCourse(courseId) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('deleting course');
            return this.coursesDB.deleteCourse(courseId);
        });
    }
    createCourse(course) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('creating course');
            return this.coursesDB.addCourse(course);
        });
    }
};
__decorate([
    Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CoursesController.prototype, "findAllCourses", null);
__decorate([
    Put(':courseId'),
    __param(0, Param('courseId')),
    __param(1, Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], CoursesController.prototype, "updateCourse", null);
__decorate([
    Delete(':courseId'),
    __param(0, Param('coursseId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CoursesController.prototype, "deleteCourse", null);
__decorate([
    Post(),
    __param(0, Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CoursesController.prototype, "createCourse", null);
CoursesController = __decorate([
    Controller('courses'),
    __metadata("design:paramtypes", [CoursesRepository])
], CoursesController);
export { CoursesController };
//# sourceMappingURL=courses.controller.js.map