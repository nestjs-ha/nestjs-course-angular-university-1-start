var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
let CoursesRepository = class CoursesRepository {
    constructor(courseModel) {
        this.courseModel = courseModel;
        // constructor(@InjectModel('Course') private courseModel: Model<Course> ) {
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.courseModel.find();
        });
    }
    // syntax: filter, update, options
    updateCourse(courseId, changes) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.courseModel.findOneAndUpdate({ _id: courseId }, changes, { new: true });
        });
    }
    deleteCourse(courseId) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.courseModel.deleteOne({ _id: courseId });
        });
    }
    addCourse(course) {
        return __awaiter(this, void 0, void 0, function* () {
            // create an in memory object with generated object id
            const newCourse = new this.courseModel(course);
            yield newCourse.save();
            return newCourse.toObject({ versionKey: false });
        });
    }
};
CoursesRepository = __decorate([
    Injectable(),
    __param(0, Inject('COURSE_MODEL')),
    __metadata("design:paramtypes", [typeof (_a = typeof Model !== "undefined" && Model) === "function" ? _a : Object])
], CoursesRepository);
export { CoursesRepository };
//# sourceMappingURL=courses.repository.js.map