import { LessonSchema } from '../schemas/lessons.schema';
export const LessonProviders = [
    {
        provide: 'LESSON_MODEL',
        useFactory: (connection) => connection.model('Lesson', LessonSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
//# sourceMappingURL=lesson.providers.js.map