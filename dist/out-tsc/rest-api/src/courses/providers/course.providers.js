import { CourseSchema } from '../schemas/courses.schema';
export const CourseProviders = [
    {
        provide: 'COURSE_MODEL',
        useFactory: (connection) => connection.model('Course', CourseSchema),
        inject: ['DATABASE_CONNECTION'],
    },
];
//# sourceMappingURL=course.providers.js.map