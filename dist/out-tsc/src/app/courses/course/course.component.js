var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { concatMap, filter, switchMap } from 'rxjs/operators';
import { CoursesHttpService } from '../services/courses-http.service';
import { MatPaginator } from '@angular/material';
let CourseComponent = class CourseComponent {
    constructor(coursesService, route) {
        this.coursesService = coursesService;
        this.route = route;
        this.displayedColumns = ['seqNo', 'description', 'duration'];
        this.currentPage = 0;
    }
    ngOnInit() {
        const courseUrl = this.route.snapshot.paramMap.get("courseUrl");
        this.course$ = this.coursesService.findCourseByUrl(courseUrl);
        this.loadLessonsPage();
    }
    ngAfterViewInit() {
        this.paginators.changes
            .pipe(filter(paginators => paginators.length > 0), switchMap(paginators => paginators.first.page))
            .subscribe((page) => {
            this.currentPage = page.pageIndex;
            this.loadLessonsPage();
        });
    }
    loadLessonsPage() {
        console.log("current page", this.currentPage);
        this.lessons$ = this.course$.pipe(concatMap(course => this.coursesService.findLessons(course._id, this.currentPage, 3)));
    }
};
__decorate([
    ViewChildren(MatPaginator),
    __metadata("design:type", QueryList)
], CourseComponent.prototype, "paginators", void 0);
__decorate([
    ViewChild(MatPaginator, { static: false }),
    __metadata("design:type", MatPaginator)
], CourseComponent.prototype, "paginator", void 0);
CourseComponent = __decorate([
    Component({
        selector: 'course',
        templateUrl: './course.component.html',
        styleUrls: ['./course.component.css']
    }),
    __metadata("design:paramtypes", [CoursesHttpService,
        ActivatedRoute])
], CourseComponent);
export { CourseComponent };
//# sourceMappingURL=course.component.js.map