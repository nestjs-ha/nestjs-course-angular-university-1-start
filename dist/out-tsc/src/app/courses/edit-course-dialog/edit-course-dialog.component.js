var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';
import { CoursesHttpService } from '../services/courses-http.service';
let EditCourseDialogComponent = class EditCourseDialogComponent {
    constructor(fb, dialogRef, data, coursesService) {
        this.fb = fb;
        this.dialogRef = dialogRef;
        this.coursesService = coursesService;
        this.dialogTitle = data.dialogTitle;
        this.course = data.course;
        this.mode = data.mode;
        const formControls = {
            description: ['', Validators.required],
            category: ['', Validators.required],
            longDescription: ['', Validators.required],
            promo: [false, []]
        };
        if (this.mode == 'update') {
            this.form = this.fb.group(formControls);
            this.form.patchValue(Object.assign({}, data.course));
        }
        else if (this.mode == 'create') {
            this.form = this.fb.group(Object.assign({}, formControls, { url: ['', Validators.required], iconUrl: ['', Validators.required] }));
        }
    }
    onClose() {
        this.dialogRef.close();
    }
    onSave() {
        const changes = Object.assign({}, this.form.value);
        if (this.mode == 'update') {
            this.coursesService.updateCourse(this.course._id, changes)
                .subscribe(course => this.dialogRef.close(course));
        }
        else if (this.mode == "create") {
            this.coursesService.createCourse(changes)
                .subscribe(course => this.dialogRef.close(course));
        }
    }
};
EditCourseDialogComponent = __decorate([
    Component({
        selector: 'course-dialog',
        templateUrl: './edit-course-dialog.component.html',
        styleUrls: ['./edit-course-dialog.component.css']
    }),
    __param(2, Inject(MAT_DIALOG_DATA)),
    __metadata("design:paramtypes", [FormBuilder,
        MatDialogRef, Object, CoursesHttpService])
], EditCourseDialogComponent);
export { EditCourseDialogComponent };
//# sourceMappingURL=edit-course-dialog.component.js.map