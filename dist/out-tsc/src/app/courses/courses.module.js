var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { CoursesCardListComponent } from './courses-card-list/courses-card-list.component';
import { EditCourseDialogComponent } from './edit-course-dialog/edit-course-dialog.component';
import { CoursesHttpService } from './services/courses-http.service';
import { CourseComponent } from './course/course.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { ReactiveFormsModule } from '@angular/forms';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
export const coursesRoutes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: ':courseUrl',
        component: CourseComponent
    }
];
let CoursesModule = class CoursesModule {
    constructor() {
    }
};
CoursesModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            MatButtonModule,
            MatIconModule,
            MatCardModule,
            MatTabsModule,
            MatInputModule,
            MatTableModule,
            MatPaginatorModule,
            MatSortModule,
            MatProgressSpinnerModule,
            MatSlideToggleModule,
            MatDialogModule,
            MatSelectModule,
            MatDatepickerModule,
            MatMomentDateModule,
            ReactiveFormsModule,
            RouterModule.forChild(coursesRoutes)
        ],
        declarations: [
            HomeComponent,
            CoursesCardListComponent,
            EditCourseDialogComponent,
            CourseComponent
        ],
        exports: [
            HomeComponent,
            CoursesCardListComponent,
            EditCourseDialogComponent,
            CourseComponent
        ],
        entryComponents: [EditCourseDialogComponent],
        providers: [
            CoursesHttpService
        ]
    }),
    __metadata("design:paramtypes", [])
], CoursesModule);
export { CoursesModule };
//# sourceMappingURL=courses.module.js.map