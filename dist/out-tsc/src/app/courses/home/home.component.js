var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { compareCourses } from '../../../../shared/course';
import { defaultDialogConfig } from '../shared/default-dialog-config';
import { EditCourseDialogComponent } from '../edit-course-dialog/edit-course-dialog.component';
import { MatDialog } from '@angular/material';
import { map, shareReplay } from 'rxjs/operators';
import { CoursesHttpService } from '../services/courses-http.service';
let HomeComponent = class HomeComponent {
    constructor(dialog, coursesHttpService) {
        this.dialog = dialog;
        this.coursesHttpService = coursesHttpService;
    }
    ngOnInit() {
        this.reload();
    }
    reload() {
        const courses$ = this.coursesHttpService.findAllCourses()
            .pipe(map(courses => courses.sort(compareCourses)), shareReplay());
        this.loading$ = courses$.pipe(map(courses => !!courses));
        this.beginnerCourses$ = courses$
            .pipe(map(courses => courses.filter(course => course.category == 'BEGINNER')));
        this.advancedCourses$ = courses$
            .pipe(map(courses => courses.filter(course => course.category == 'ADVANCED')));
        this.promoTotal$ = courses$
            .pipe(map(courses => courses.filter(course => course.promo).length));
    }
    onAddCourse() {
        const dialogConfig = defaultDialogConfig();
        dialogConfig.data = {
            dialogTitle: "Create Course",
            mode: 'create'
        };
        this.dialog.open(EditCourseDialogComponent, dialogConfig)
            .afterClosed()
            .subscribe(data => {
            if (data) {
                this.reload();
            }
        });
    }
};
HomeComponent = __decorate([
    Component({
        selector: 'home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.css']
    }),
    __metadata("design:paramtypes", [MatDialog,
        CoursesHttpService])
], HomeComponent);
export { HomeComponent };
//# sourceMappingURL=home.component.js.map