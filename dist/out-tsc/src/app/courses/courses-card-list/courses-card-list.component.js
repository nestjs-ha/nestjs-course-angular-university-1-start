var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { EditCourseDialogComponent } from "../edit-course-dialog/edit-course-dialog.component";
import { defaultDialogConfig } from '../shared/default-dialog-config';
import { CoursesHttpService } from '../services/courses-http.service';
let CoursesCardListComponent = class CoursesCardListComponent {
    constructor(dialog, coursesService) {
        this.dialog = dialog;
        this.coursesService = coursesService;
        this.courseChanged = new EventEmitter();
    }
    ngOnInit() {
    }
    editCourse(course) {
        const dialogConfig = defaultDialogConfig();
        dialogConfig.data = {
            dialogTitle: "Edit Course",
            course,
            mode: 'update'
        };
        this.dialog.open(EditCourseDialogComponent, dialogConfig)
            .afterClosed()
            .subscribe(() => this.courseChanged.emit());
    }
    onDeleteCourse(course) {
        this.coursesService.deleteCourse(course._id)
            .subscribe(() => this.courseChanged.emit());
    }
};
__decorate([
    Input(),
    __metadata("design:type", Array)
], CoursesCardListComponent.prototype, "courses", void 0);
__decorate([
    Output(),
    __metadata("design:type", Object)
], CoursesCardListComponent.prototype, "courseChanged", void 0);
CoursesCardListComponent = __decorate([
    Component({
        selector: 'courses-card-list',
        templateUrl: './courses-card-list.component.html',
        styleUrls: ['./courses-card-list.component.css']
    }),
    __metadata("design:paramtypes", [MatDialog,
        CoursesHttpService])
], CoursesCardListComponent);
export { CoursesCardListComponent };
//# sourceMappingURL=courses-card-list.component.js.map